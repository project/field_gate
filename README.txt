Field Gate Module


INTRODUCTION
=============

The Field Gate module let's you configure a field so that it has to be set to a specified value to see the fields below it. Configuration happens via the field settings on the form display. You can specify a message to show for when a different value is set. Currently only works for the radio buttons widget.

Works with field groups.

Note: The hiding / showing of fields is achieved using styles and javascript and does not add extra validation to ensure that the gate is respected at the form validation level. This should not be used where you a require a more secure approach. MR's welcome!


REQUIREMENTS
============

No special requirements.


INSTALLATION
============

Install as you would normally install a contributed Drupal module. See: https://www.drupal.org/documentation/install/modules-themes/modules-8


MAINTAINERS
===========

Current Maintainers:
* Michael Welford - https://www.drupal.org/u/mikejw

This project has been sponsored by:
* The University of Adelaide - https://www.drupal.org/university-of-adelaide
  Visit: http://www.adelaide.edu.au
